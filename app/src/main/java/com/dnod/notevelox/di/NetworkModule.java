package com.dnod.notevelox.di;

import android.content.Context;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.source.AppDataSource;
import com.dnod.notevelox.data.source.DataSourceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    public static Gson provideGson() {
        return new GsonBuilder()
                .create();
    }

    @Provides
    @Singleton
    @Endpoint
    public static String provideEndpoint(Context context) {
        return context.getString(R.string.api_endpoint);
    }

    @Provides
    public static AppDataSource provideDataSource(DataSourceImpl dataSource) {
        return dataSource;
    }
}
