package com.dnod.notevelox;

import android.content.Context;

import com.dnod.notevelox.data.util.PreferencesHelper;
import com.dnod.notevelox.di.ApplicationComponent;
import com.dnod.notevelox.di.DaggerApplicationComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class NoteVeloxApplication extends DaggerApplication {
    private static NoteVeloxApplication sInstance;

    public static Context getInstance() {
        return sInstance;
    }

    private ApplicationComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        PreferencesHelper.init(this);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        mAppComponent = DaggerApplicationComponent.builder().application(this).build();
        mAppComponent.inject(this);
        return mAppComponent;
    }

    public static ApplicationComponent getApplicationInjector() {
        return sInstance.mAppComponent;
    }
}