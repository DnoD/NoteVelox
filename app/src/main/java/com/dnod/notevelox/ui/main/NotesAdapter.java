package com.dnod.notevelox.ui.main;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.databinding.ItemNoteBinding;

import java.util.LinkedList;
import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.Holder> {

    public interface ItemActionListener {
        void onItemClick(Note note);

        void onItemEdit(Note note);

        void onItemDelete(Note note);
    }

    private final List<Note> mData;
    private final LayoutInflater layoutInflater;
    private ItemActionListener onItemActionListener;

    public NotesAdapter(Context context) {
        this.layoutInflater = LayoutInflater.from(context);
        this.mData = new LinkedList<>();
    }

    public NotesAdapter setOnItemActionListener(ItemActionListener onItemActionListener) {
        this.onItemActionListener = onItemActionListener;
        return this;
    }

    public void addAll(List<Note> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void remove(Note note) {
        for (int i = 0; i < getItemCount(); i++) {
            if (mData.get(i).getId().equals(note.getId())) {
                mData.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(DataBindingUtil.inflate(layoutInflater,
                R.layout.item_note, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Note item = mData.get(position);
        holder.bindingObject.setModel(item);
        holder.bindingObject.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    final class Holder extends RecyclerView.ViewHolder implements View.OnClickListener,
            PopupMenu.OnMenuItemClickListener {
        ItemNoteBinding bindingObject;
        PopupMenu actionsMenu;

        public Holder(ItemNoteBinding binding) {
            super(binding.getRoot());
            bindingObject = binding;
            bindingObject.getRoot().setOnClickListener(this);
            bindingObject.btnCtxMenu.setOnClickListener(this);
            this.actionsMenu = new PopupMenu(layoutInflater.getContext(), binding.btnCtxMenu);
            this.actionsMenu.setOnMenuItemClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_ctx_menu:
                    actionsMenu.getMenu().clear();
                    actionsMenu.inflate(R.menu.menu_note);
                    actionsMenu.show();
                    break;
                default:
                    if (onItemActionListener != null) {
                        onItemActionListener.onItemClick(mData.get(getAdapterPosition()));
                    }
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    if (onItemActionListener != null) {
                        onItemActionListener.onItemEdit(mData.get(getAdapterPosition()));
                    }
                    return true;
                case R.id.action_delete:
                    if (onItemActionListener != null) {
                        onItemActionListener.onItemDelete(mData.get(getAdapterPosition()));
                    }
                    return true;
            }
            return false;
        }
    }
}