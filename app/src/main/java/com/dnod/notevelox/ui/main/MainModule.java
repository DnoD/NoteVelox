package com.dnod.notevelox.ui.main;

import com.dnod.notevelox.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainModule {

    @FragmentScoped
    @Binds
    abstract MainContract.Presenter mainPresenter(MainPresenter presenter);
}
