package com.dnod.notevelox.ui.settings;

import com.dnod.notevelox.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SettingsModule {

    @FragmentScoped
    @Binds
    abstract SettingsContract.Presenter settingsPresenter(SettingsPresenter presenter);
}
