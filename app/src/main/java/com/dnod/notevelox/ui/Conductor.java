package com.dnod.notevelox.ui;

public interface Conductor<T> {

    void goTo(T t);

    void goBack();
}
