package com.dnod.notevelox.ui.settings;

import com.dnod.notevelox.ScreeView;
import com.dnod.notevelox.ScreenPresenter;

public interface SettingsContract {
    interface View extends ScreeView<Presenter> {
    }

    interface Presenter extends ScreenPresenter<View> {
    }
}
