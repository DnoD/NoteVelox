package com.dnod.notevelox.ui.details;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.databinding.FragmentNoteDetailsBinding;
import com.dnod.notevelox.ui.Conductor;
import com.dnod.notevelox.ui.base.BaseFragment;

import javax.inject.Inject;

public class DetailsFragment extends BaseFragment<DetailsContract.Presenter> implements DetailsContract.View {

    private static final String PROVIDED_NOTE = "provided_note";

    public static BaseFragment createInstance() {
        return createInstance(null);
    }

    public static BaseFragment createInstance(@Nullable Note note) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        if (note != null) {
            args.putParcelable(PROVIDED_NOTE, note);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    Conductor<BaseFragment> conductor;
    @Inject
    DetailsContract.Presenter presenter;

    private FragmentNoteDetailsBinding mBindingObject;
    private Note mProvidedNote;

    @Inject
    public DetailsFragment() {
    }

    @NonNull
    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
        mBindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_note_details, container, false);
        mBindingObject.toolbar.inflateMenu(R.menu.menu_note_details);
        if (getArguments().containsKey(PROVIDED_NOTE)) {
            mProvidedNote = getArguments().getParcelable(PROVIDED_NOTE);
            mBindingObject.toolbar.setTitle(mProvidedNote.getId());
            mBindingObject.description.setText(mProvidedNote.getTitle());
        } else {
            mBindingObject.toolbar.getMenu().findItem(R.id.action_delete).setVisible(false);
        }
        mBindingObject.toolbar.setNavigationOnClickListener(view -> {
            hideKeyboard();
            conductor.goBack();
        });
        mBindingObject.toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_save:
                    if (mBindingObject.description.getText().toString().isEmpty()) {
                        showMessage(R.string.error_empty_note_description);
                        return true;
                    }
                    hideKeyboard();
                    Note note = new Note(mBindingObject.description.getText().toString());
                    if (mProvidedNote != null) {
                        note.setId(mProvidedNote.getId());
                    }
                    presenter.save(note);
                    return true;
                case R.id.action_delete:
                    hideKeyboard();
                    presenter.delete(mProvidedNote);
                    break;
            }
            return false;
        });
        return mBindingObject.getRoot();
    }

    @Override
    public void notifyNoteSaveFailure() {
        showMessage(R.string.error_save_note);
    }

    @Override
    public void notifyNoteDeleteFailure() {
        showMessage(R.string.error_delete_note);
    }

    @Override
    public void notifyNoteSaved(Note note) {
        mProvidedNote = note;
        showMessage(R.string.message_note_saved);
        mBindingObject.getRoot().postDelayed(() -> conductor.goBack(), 1000);
    }

    @Override
    public void notifyNoteDeleted() {
        showMessage(R.string.message_note_deleted);
        mBindingObject.getRoot().postDelayed(() -> conductor.goBack(), 1000);
    }

    @Override
    public String getScreenTag() {
        return DetailsFragment.class.getSimpleName();
    }

    @Nullable
    @Override
    protected DetailsContract.Presenter providePresenter() {
        return presenter;
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mBindingObject.description.getWindowToken(), 0);
    }
}
