package com.dnod.notevelox.ui.settings;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.util.PreferencesHelper;
import com.dnod.notevelox.databinding.FragmentSettingsBinding;
import com.dnod.notevelox.manager.LocaleManager;
import com.dnod.notevelox.ui.Conductor;
import com.dnod.notevelox.ui.base.BaseFragment;

import javax.inject.Inject;

public class SettingsFragment extends BaseFragment<SettingsContract.Presenter> implements SettingsContract.View {

    public static BaseFragment createInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    Conductor<BaseFragment> conductor;
    @Inject
    SettingsContract.Presenter presenter;

    private FragmentSettingsBinding mBindingObject;

    @Inject
    public SettingsFragment() {
    }

    @NonNull
    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
        mBindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        setupToolbar();
        setupLanguages();
        return mBindingObject.getRoot();
    }

    @Override
    public String getScreenTag() {
        return SettingsFragment.class.getSimpleName();
    }

    @Nullable
    @Override
    protected SettingsContract.Presenter providePresenter() {
        return presenter;
    }

    private void setupToolbar() {
        mBindingObject.toolbar.inflateMenu(R.menu.menu_settings);
        mBindingObject.toolbar.setNavigationOnClickListener(view -> conductor.goBack());
        mBindingObject.toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_save:
                    String[] locales = getResources().getStringArray(R.array.locales);
                    String selectedLocale = locales[mBindingObject.languages.getSelectedItemPosition()];
                    PreferencesHelper.saveLocale(selectedLocale);
                    LocaleManager.setNewLocale(getContext(), selectedLocale);
                    showMessage(R.string.message_settings_saved);
                    mBindingObject.getRoot().postDelayed(() -> conductor.goBack(), 1000);
                    return true;
            }
            return false;
        });
    }

    private void setupLanguages() {
        String[] names = getResources().getStringArray(R.array.locales_names);
        String[] locales = getResources().getStringArray(R.array.locales);
        ArrayAdapter adapter = new ArrayAdapter<>(getContext(), R.layout.simple_spinner_item,
                names);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mBindingObject.languages.setAdapter(adapter);
        String selectedLocale = PreferencesHelper.getLocale();
        int selectedPos = -1;
        for (int i = 0; i < locales.length && selectedPos < 0; i++) {
            if (locales[i].equals(selectedLocale)) {
                selectedPos = i;
            }
        }
        mBindingObject.languages.setSelection(selectedPos);
    }
}
