package com.dnod.notevelox.ui.base;

import android.os.Bundle;
import android.support.annotation.AnimRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.notevelox.R;
import com.dnod.notevelox.ScreeView;
import com.dnod.notevelox.ScreenPresenter;
import com.dnod.notevelox.ui.BackInterceptor;

import dagger.android.support.DaggerFragment;

public abstract class BaseFragment<T extends ScreenPresenter> extends DaggerFragment implements BackInterceptor, ScreeView<T> {

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        T presenter = providePresenter();
        if (presenter != null) {
            presenter.takeView(this);
        }
        return bindView(inflater, container, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        T presenter = providePresenter();
        if (presenter != null) {
            presenter.dropView();
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        T presenter = providePresenter();
        if (presenter != null) {
            presenter.destroyScreen();
        }
        super.onDestroy();
    }

    @Override
    public void showMessage(int message) {
        showMessage(getString(message));
    }

    @Override
    public void showMessage(String message) {
        makeSnackBar(message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean handleBackPress() {
        return false;
    }

    @AnimRes
    public int getAppearanceAnimation() {
        return R.anim.slide_in_right;
    }

    @AnimRes
    public int getDisappearanceAnimation() {
        return R.anim.slide_out_left;
    }

    @AnimRes
    public int getPopAppearanceAnimation() {
        return R.anim.slide_in_left;
    }

    @AnimRes
    public int getPopDisappearanceAnimation() {
        return R.anim.slide_out_right;
    }

    protected Snackbar makeSnackBar(@StringRes int message, int duration) {
        return makeSnackBar(getString(message), duration);
    }

    protected Snackbar makeSnackBar(@NonNull String message, int duration) {
        return Snackbar.make(getView(), message, duration);
    }

    public abstract String getScreenTag();

    @Nullable
    protected abstract T providePresenter();

    @NonNull
    protected abstract View bindView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);
}
