package com.dnod.notevelox.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.databinding.FragmentMainBinding;
import com.dnod.notevelox.ui.Conductor;
import com.dnod.notevelox.ui.base.BaseFragment;
import com.dnod.notevelox.ui.decor.DividerItemDecoration;
import com.dnod.notevelox.ui.details.DetailsFragment;
import com.dnod.notevelox.ui.settings.SettingsFragment;

import java.util.List;

import javax.inject.Inject;

public class MainFragment extends BaseFragment<MainContract.Presenter> implements MainContract.View, NotesAdapter.ItemActionListener {

    public static BaseFragment createInstance() {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    Conductor<BaseFragment> conductor;
    @Inject
    MainContract.Presenter presenter;

    private FragmentMainBinding mBindingObject;
    private NotesAdapter mAdapter;

    @Inject
    public MainFragment() {
    }

    @NonNull
    @Override
    protected View bindView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
        mBindingObject = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);
        mBindingObject.toolbar.inflateMenu(R.menu.menu_main);
        mBindingObject.toolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_settings:
                    conductor.goTo(SettingsFragment.createInstance());
                    return true;
            }
            return false;
        });
        mBindingObject.fab.setOnClickListener(view -> {
            conductor.goTo(DetailsFragment.createInstance());
        });
        mBindingObject.notes.setLayoutManager(new LinearLayoutManager(getContext()));
        mBindingObject.notes.addItemDecoration(new DividerItemDecoration(getContext(), R.drawable.simple_divider));
        updateEmptyState();
        presenter.loadData();
        return mBindingObject.getRoot();
    }

    @Override
    public void showData(List<Note> notes) {
        mAdapter = new NotesAdapter(getContext()).setOnItemActionListener(this);
        mBindingObject.notes.setAdapter(mAdapter);
        mAdapter.addAll(notes);
        updateEmptyState();
    }

    @Override
    public void onItemClick(Note note) {
        conductor.goTo(DetailsFragment.createInstance(note));
    }

    @Override
    public void onItemEdit(Note note) {
        conductor.goTo(DetailsFragment.createInstance(note));
    }

    @Override
    public void onItemDelete(Note note) {
        presenter.delete(note);
    }

    @Override
    public void notifyNoteDeleted(Note note) {
        mAdapter.remove(note);
        showMessage(R.string.message_note_deleted);
        updateEmptyState();
    }

    @Override
    public void notifyNoteDeleteFailure() {
        showMessage(R.string.error_delete_note);
    }

    @Override
    public String getScreenTag() {
        return MainFragment.class.getSimpleName();
    }

    @Nullable
    @Override
    protected MainContract.Presenter providePresenter() {
        return presenter;
    }

    public int getAppearanceAnimation() {
        return R.anim.empty;
    }

    public int getDisappearanceAnimation() {
        return R.anim.empty;
    }

    public int getPopAppearanceAnimation() {
        return R.anim.empty;
    }

    public int getPopDisappearanceAnimation() {
        return R.anim.empty;
    }

    private void updateEmptyState() {
        mBindingObject.stateMessage.setVisibility(View.VISIBLE);
        if (mAdapter == null) {
            mBindingObject.stateMessage.setText(R.string.message_loading_content);
        } else if (mAdapter.getItemCount() > 0) {
            mBindingObject.stateMessage.setVisibility(View.GONE);
        } else {
            mBindingObject.stateMessage.setText(R.string.message_no_records);
        }
    }
}
