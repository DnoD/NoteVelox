package com.dnod.notevelox.ui.settings;

import com.dnod.notevelox.ui.base.BaseScreenPresenter;

import javax.inject.Inject;

public final class SettingsPresenter extends BaseScreenPresenter<SettingsContract.View> implements SettingsContract.Presenter {

    @Inject
    public SettingsPresenter() {
    }

    @Override
    protected void onViewDetached() {
    }

    @Override
    protected void onViewAttached() {
    }

    @Override
    protected void onScreenDestroyed() {
    }
}
