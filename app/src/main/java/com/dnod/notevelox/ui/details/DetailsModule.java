package com.dnod.notevelox.ui.details;

import com.dnod.notevelox.di.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DetailsModule {

    @FragmentScoped
    @Binds
    abstract DetailsContract.Presenter detailsPresenter(DetailsPresenter presenter);
}
