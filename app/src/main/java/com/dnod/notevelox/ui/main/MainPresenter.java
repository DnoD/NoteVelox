package com.dnod.notevelox.ui.main;

import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.data.source.AppDataSource;
import com.dnod.notevelox.ui.base.BaseScreenPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public final class MainPresenter extends BaseScreenPresenter<MainContract.View> implements MainContract.Presenter {

    private final AppDataSource mAppDataSource;

    @Inject
    public MainPresenter(AppDataSource dataSource) {
        this.mAppDataSource = dataSource;
    }

    @Override
    public void loadData() {
        viewSubscriptions.add(mAppDataSource.listNotes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(data -> {
                    getView().showData(data);
                }, throwable -> getView().showMessage(throwable.getLocalizedMessage())));
    }

    @Override
    public void delete(Note note) {
        screenSubscriptions.add(mAppDataSource.deleteNote(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (isViewAttached()) {
                        if (result) {
                            getView().notifyNoteDeleted(note);
                        } else {
                            getView().notifyNoteDeleteFailure();
                        }
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().notifyNoteDeleteFailure();
                    }
                }));
    }

    @Override
    protected void onViewDetached() {
    }

    @Override
    protected void onViewAttached() {
    }

    @Override
    protected void onScreenDestroyed() {
    }
}
