package com.dnod.notevelox.ui.details;

import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.data.source.AppDataSource;
import com.dnod.notevelox.ui.base.BaseScreenPresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public final class DetailsPresenter extends BaseScreenPresenter<DetailsContract.View> implements DetailsContract.Presenter {

    private final AppDataSource mAppDataSource;

    @Inject
    public DetailsPresenter(AppDataSource dataSource) {
        this.mAppDataSource = dataSource;
    }

    @Override
    public void save(Note note) {
        if (note.getId() == null) {
            screenSubscriptions.add(mAppDataSource.addNote(note)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        if (isViewAttached()) {
                            if (result != null) {
                                getView().notifyNoteSaved(result);
                            } else {
                                getView().notifyNoteSaveFailure();
                            }
                        }
                    }, throwable -> {
                        if (isViewAttached()) {
                            getView().notifyNoteSaveFailure();
                        }
                    }));
        } else {
            screenSubscriptions.add(mAppDataSource.updateNote(note)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        if (isViewAttached()) {
                            if (result != null) {
                                getView().notifyNoteSaved(result);
                            } else {
                                getView().notifyNoteSaveFailure();
                            }
                        }
                    }, throwable -> {
                        if (isViewAttached()) {
                            getView().notifyNoteSaveFailure();
                        }
                    }));
        }
    }

    @Override
    public void delete(Note note) {
        screenSubscriptions.add(mAppDataSource.deleteNote(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (isViewAttached()) {
                        if (result) {
                            getView().notifyNoteDeleted();
                        } else {
                            getView().notifyNoteDeleteFailure();
                        }
                    }
                }, throwable -> {
                    if (isViewAttached()) {
                        getView().notifyNoteDeleteFailure();
                    }
                }));
    }

    @Override
    protected void onViewDetached() {
    }

    @Override
    protected void onViewAttached() {
    }

    @Override
    protected void onScreenDestroyed() {
    }
}
