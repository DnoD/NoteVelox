package com.dnod.notevelox.ui.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.dnod.notevelox.R;
import com.dnod.notevelox.data.util.PreferencesHelper;
import com.dnod.notevelox.databinding.ActivityMainBinding;
import com.dnod.notevelox.manager.LocaleManager;
import com.dnod.notevelox.ui.Conductor;
import com.dnod.notevelox.ui.base.BaseActivity;
import com.dnod.notevelox.ui.base.BaseFragment;

public class MainActivity extends BaseActivity implements Conductor<BaseFragment> {

    private ActivityMainBinding mBindingObject;
    private BaseFragment mCurrFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String selectedLocale = PreferencesHelper.getLocale();
        LocaleManager.setNewLocale(this, selectedLocale);
        mBindingObject = DataBindingUtil.setContentView(this, R.layout.activity_main);
        goTo(MainFragment.createInstance());
    }

    @Override
    public void goTo(BaseFragment fragment) {
        int appearanceAnimation = fragment.getAppearanceAnimation();
        int disappearanceAnimation = fragment.getDisappearanceAnimation();
        int popAppearanceAnimation = fragment.getPopAppearanceAnimation();
        int popDisappearanceAnimation = fragment.getPopDisappearanceAnimation();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(appearanceAnimation, disappearanceAnimation,
                popAppearanceAnimation, popDisappearanceAnimation);
        if (mCurrFragment != null) {
            fragmentTransaction.addToBackStack(fragment.getScreenTag());
        }
        fragmentTransaction.replace(R.id.container, mCurrFragment = fragment, fragment.getScreenTag())
                .commit();
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    protected View getRootView() {
        return mBindingObject.getRoot();
    }
}
