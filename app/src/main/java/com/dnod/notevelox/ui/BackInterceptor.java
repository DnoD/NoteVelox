package com.dnod.notevelox.ui;

public interface BackInterceptor {

    boolean handleBackPress();
}
