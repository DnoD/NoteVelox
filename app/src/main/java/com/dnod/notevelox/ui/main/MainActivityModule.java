package com.dnod.notevelox.ui.main;

import com.dnod.notevelox.di.ActivityScoped;
import com.dnod.notevelox.di.FragmentScoped;
import com.dnod.notevelox.ui.Conductor;
import com.dnod.notevelox.ui.base.BaseFragment;
import com.dnod.notevelox.ui.details.DetailsFragment;
import com.dnod.notevelox.ui.details.DetailsModule;
import com.dnod.notevelox.ui.settings.SettingsFragment;
import com.dnod.notevelox.ui.settings.SettingsModule;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @Provides
    @ActivityScoped
    static Conductor<BaseFragment> provideConductor(MainActivity activity) {
        return activity;
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainFragment addMainFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = DetailsModule.class)
    abstract DetailsFragment addDetailsFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = SettingsModule.class)
    abstract SettingsFragment addSettingsFragment();
}
