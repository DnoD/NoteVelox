package com.dnod.notevelox.ui.details;

import com.dnod.notevelox.ScreeView;
import com.dnod.notevelox.ScreenPresenter;
import com.dnod.notevelox.data.Note;

import java.util.List;

public interface DetailsContract {
    interface View extends ScreeView<Presenter> {

        void notifyNoteSaved(Note note);

        void notifyNoteSaveFailure();

        void notifyNoteDeleted();

        void notifyNoteDeleteFailure();
    }

    interface Presenter extends ScreenPresenter<View> {

        void save(Note note);

        void delete(Note note);
    }
}
