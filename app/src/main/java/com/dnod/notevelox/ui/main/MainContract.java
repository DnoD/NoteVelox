package com.dnod.notevelox.ui.main;

import com.dnod.notevelox.ScreeView;
import com.dnod.notevelox.ScreenPresenter;
import com.dnod.notevelox.data.Note;

import java.util.List;

public interface MainContract {
    interface View extends ScreeView<Presenter> {

        void showData(List<Note> notes);

        void notifyNoteDeleted(Note note);

        void notifyNoteDeleteFailure();
    }

    interface Presenter extends ScreenPresenter<View> {

        void loadData();

        void delete(Note note);
    }
}
