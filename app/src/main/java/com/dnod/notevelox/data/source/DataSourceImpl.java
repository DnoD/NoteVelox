package com.dnod.notevelox.data.source;

import android.support.annotation.NonNull;

import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.data.api.ClientApi;
import com.dnod.notevelox.di.Endpoint;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataSourceImpl implements AppDataSource {

    private final static long CONNECT_TIMEOUT = 3;       //In secnds
    private final static int MAX_RETRY = 3;
    private final ClientApi clientApi;

    @Inject
    public DataSourceImpl(@Endpoint String endpoint, Gson gson) {

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(@NonNull Chain chain) throws IOException {
                        Request request = chain.request();
                        return proceed(chain, request, 0);
                    }

                    private Response proceed(Chain chain, Request request, int retryCount) throws IOException {
                        Response response = null;
                        try {
                            response = chain.proceed(request);
                            if (!response.isSuccessful() && retryCount < MAX_RETRY) {
                                return proceed(chain, request, ++retryCount);
                            }
                        } catch (IOException e) {
                            if (retryCount < MAX_RETRY) {
                                return proceed(chain, request, ++retryCount);
                            } else {
                                throw e;
                            }
                        } catch (OutOfMemoryError error) {
                            throw new IOException();
                        }
                        return response;
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        Retrofit retrofitClient = new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        clientApi = retrofitClient.create(ClientApi.class);
    }

    @Override
    public Observable<Note> addNote(Note note) {
        return clientApi.createNote(note)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Boolean> deleteNote(Note note) {
        return clientApi.deleteNote(note.getId())
                .subscribeOn(Schedulers.io())
                .map(response -> response.code() == HttpURLConnection.HTTP_NO_CONTENT);
    }

    @Override
    public Observable<Note> updateNote(Note note) {
        return clientApi.updateNote(note.getId(), note)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<List<Note>> listNotes() {
        return clientApi.getNotes()
                .subscribeOn(Schedulers.io());
    }
}
