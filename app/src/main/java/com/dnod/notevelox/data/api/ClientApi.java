package com.dnod.notevelox.data.api;

import com.dnod.notevelox.data.Note;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import io.reactivex.Observable;

public interface ClientApi {

    @GET("notes")
    Observable<List<Note>> getNotes();

    @POST("notes")
    Observable<Note> createNote(@Body Note model);

    @PUT("notes/{id}")
    Observable<Note> updateNote(@Path("id") String noteId, @Body Note model);

    @DELETE("notes/{id}")
    Observable<Response<Void>> deleteNote(@Path("id") String noteId);
}
