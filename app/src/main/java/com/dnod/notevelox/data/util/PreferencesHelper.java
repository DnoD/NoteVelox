package com.dnod.notevelox.data.util;

import android.content.Context;
import android.content.SharedPreferences;

public final class PreferencesHelper {
    private static final String PREFERENCES_NAME = "local_preferences";

    private static final String KEY_LOCALE = "locale";

    private static PreferencesHelper sInstance;
    private final SharedPreferences mSharedPreferences;

    private PreferencesHelper(Context context) {
        mSharedPreferences = openPreferences(context);
    }

    public static void init(Context context) {
        sInstance = new PreferencesHelper(context);
    }

    public static void saveLocale(String language) {
        sInstance.openEditor().putString(KEY_LOCALE, language).apply();
    }

    public static String getLocale() {
        return sInstance.mSharedPreferences.getString(KEY_LOCALE, "en");
    }

    private SharedPreferences openPreferences(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor openEditor() {
        return mSharedPreferences.edit();
    }
}
