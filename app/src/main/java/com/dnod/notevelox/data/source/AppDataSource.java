package com.dnod.notevelox.data.source;

import com.dnod.notevelox.data.Note;

import java.util.List;

import io.reactivex.Observable;

public interface AppDataSource {

    Observable<Note> addNote(Note note);

    Observable<Boolean> deleteNote(Note note);

    Observable<Note> updateNote(Note note);

    Observable<List<Note>> listNotes();
}
