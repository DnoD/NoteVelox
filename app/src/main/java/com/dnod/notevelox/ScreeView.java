package com.dnod.notevelox;

import android.support.annotation.StringRes;

public interface ScreeView<T extends ScreenPresenter> {

    /**
     * Show message using Snackbar
     * @param message
     */
    void showMessage(String message);

    void showMessage(@StringRes int message);
}