package com.dnod.notevelox;

import com.dnod.notevelox.data.Note;
import com.dnod.notevelox.data.source.AppDataSource;
import com.dnod.notevelox.data.source.DataSourceImpl;
import com.dnod.notevelox.ui.details.DetailsContract;
import com.dnod.notevelox.ui.details.DetailsPresenter;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(JUnit4.class)
public class UnitTest {

    protected static AppDataSource mockDataSource;

    @BeforeClass
    public static void setup() {
        mockDataSource = mock(DataSourceImpl.class);
    }

    @BeforeClass
    public static void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                // this prevents StackOverflowErrors when scheduling with a delay
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Test
    public void testCreate() {
        final String title = "Some title";
        final String id = "1";

        DetailsContract.Presenter presenter = new DetailsPresenter(mockDataSource);
        DetailsContract.View view = mock(DetailsContract.View.class);
        presenter.takeView(view);

        ArgumentCaptor<Note> noteCapture = ArgumentCaptor.forClass(Note.class);

        final Note resultNote = new Note(title).setId(id);
        final Note newNote = new Note(title);
        Observable<Note> result = Observable.just(resultNote);
        when(mockDataSource.addNote(newNote)).thenReturn(result);

        presenter.save(newNote);

        verify(view, times(1)).notifyNoteSaved(noteCapture.capture());

        assertEquals(noteCapture.getValue().getId(), resultNote.getId());
        assertEquals(noteCapture.getValue().getTitle(), newNote.getTitle());

        result.test().assertSubscribed()
                .assertNoErrors()
                .assertValue(resultNote)
                .assertComplete();
    }
}
